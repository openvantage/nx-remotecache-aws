import defaultTaskRunner from '@nrwl/workspace/tasks-runners/default';
import {join} from 'path';
// @ts-ignore
import * as s3 from '@auth0/s3';

export default function runner(
    tasks: Parameters<typeof defaultTaskRunner>[0],
    options: Parameters<typeof defaultTaskRunner>[1] & { bucket?: string, accessKeyId?: string, secretAccessKey?: string, region?: string },
    context: Parameters<typeof defaultTaskRunner>[2],
) {
    const client = s3.createClient({
        maxAsyncS3: 20,     // this is the default
        s3RetryCount: 3,    // this is the default
        s3RetryDelay: 1000, // this is the default
        multipartUploadThreshold: 20971520, // this is the default (20 MB)
        multipartUploadSize: 15728640, // this is the default (15 MB)
        s3Options: {
            accessKeyId: options.accessKeyId,
            secretAccessKey: options.secretAccessKey,
            region: options.region
            // any other options are passed to new AWS.S3()
            // See: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/Config.html#constructor-property
        },
    });

    if (!options.bucket) {
        throw new Error('missing bucket property in runner options. Please update nx.json');
    }
    return defaultTaskRunner(tasks, {...options, remoteCache: {retrieve, store}}, context);

    async function retrieve(hash: string, cacheDirectory: string): Promise<boolean> {
        console.log('Syncing cache with AWS - ' + hash);
        try {
            const paramsCommit = {
                s3Params: {
                    Bucket: options.bucket,
                    Key: `${hash}.commit`
                },
                localFile: join(cacheDirectory, `${hash}.commit`),
            };
            const paramsDir = {
                s3Params: {
                    Bucket: options.bucket,
                    Prefix: `${hash}`
                },
                localDir: join(cacheDirectory, `${hash}`),
            };
            const paramsTerminal = {
                s3Params: {
                    Bucket: options.bucket,
                    Key: `terminalOutputs/${hash}`
                },
                localFile: join(cacheDirectory, `terminalOutputs/${hash}`),
            };

            const tasks: Promise<boolean>[] = [];
            tasks.push(new Promise(resolve => {
                const uploderCommit = client.downloadFile(paramsCommit);
                uploderCommit.on('end', function () {
                    console.log("Part1 - downloading...");
                    resolve(true);
                });
                uploderCommit.on('error', function (err: any) {
                    console.error(err);
                    resolve(true);
                });
            }));
            tasks.push(new Promise(resolve => {
                const uploderTerminal = client.downloadFile(paramsTerminal);
                uploderTerminal.on('end', function () {
                    console.log("Part2 - downloading...");
                    resolve(true);
                });
                uploderTerminal.on('error', function (err: any) {
                    console.error(err);
                    resolve(true);
                });
            }));
            tasks.push(new Promise(resolve => {
                const uploderDir = client.downloadDir(paramsDir);
                uploderDir.on('end', function () {
                    console.log("Part3 - downloading...");
                    resolve(true);
                });
                uploderDir.on('error', function (err: any) {
                    console.error(err);
                    resolve(true);
                });
            }));

            await Promise.all(tasks);

            console.log('Cache finished downloading.');
            return true;
        } catch (e) {
            console.log(e);
            console.log(`WARNING: failed to download cache from ${options.bucket}: ${e.message}`);
            return false;
        }
    }

    async function store(hash: string, cacheDirectory: string): Promise<boolean> {
        try {
            console.log('Syncing cache with AWS - ' + hash);
            const paramsDir = {
                deleteRemoved: false,
                s3Params: {
                    Bucket: options.bucket,
                    Prefix: `${hash}`
                },
                localDir: join(cacheDirectory, `${hash}`),
            }
            const paramsCommit = {
                deleteRemoved: false,
                s3Params: {
                    Bucket: options.bucket,
                    Key: `${hash}.commit`
                },
                localFile: join(cacheDirectory, `${hash}.commit`),
            }
            const paramsTerminal = {
                deleteRemoved: false,
                s3Params: {
                    Bucket: options.bucket,
                    Key: `terminalOutputs/${hash}`
                },
                localFile: join(cacheDirectory, `terminalOutputs/${hash}`),
            }

            const tasks: Promise<any>[] = [];
            tasks.push(new Promise(resolve => {
                const uploderCommit = client.uploadFile(paramsCommit);
                uploderCommit.on('end', function () {
                    console.log("Part1 - uploading...");
                    resolve(true);
                });
                uploderCommit.on('error', function (err: any) {
                    console.error(err);
                    resolve(true);
                });
            }));
            tasks.push(new Promise(resolve => {
                const uploderTerminal = client.uploadFile(paramsTerminal);
                uploderTerminal.on('end', function () {
                    console.log("Part2 - uploading...");
                    resolve(true);
                });
                uploderTerminal.on('error', function (err: any) {
                    console.error(err);
                    resolve(true);
                });
            }));
            tasks.push(new Promise(resolve => {
                const uploderDir = client.uploadDir(paramsDir);
                uploderDir.on('end', function () {
                    console.log("Part3 - uploading...");
                    resolve(true);
                });
                uploderDir.on('error', function (err: any) {
                    console.error(err);
                    resolve(true);
                });
            }));

            await Promise.all(tasks);

            console.log(`Cache uploaded`);
            return true;
        } catch (e) {
            console.log(`WARNING: failed to upload cache to ${options.bucket}: ${e.message}`);
            return false;
        }
    }
}

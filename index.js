"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var default_1 = require("@nrwl/workspace/tasks-runners/default");
var path_1 = require("path");
// @ts-ignore
var s3 = require("@auth0/s3");
function runner(tasks, options, context) {
    var client = s3.createClient({
        maxAsyncS3: 20,
        s3RetryCount: 3,
        s3RetryDelay: 1000,
        multipartUploadThreshold: 20971520,
        multipartUploadSize: 15728640,
        s3Options: {
            accessKeyId: options.accessKeyId,
            secretAccessKey: options.secretAccessKey,
            region: options.region
            // any other options are passed to new AWS.S3()
            // See: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/Config.html#constructor-property
        }
    });
    if (!options.bucket) {
        throw new Error('missing bucket property in runner options. Please update nx.json');
    }
    return default_1["default"](tasks, __assign(__assign({}, options), { remoteCache: { retrieve: retrieve, store: store } }), context);
    function retrieve(hash, cacheDirectory) {
        return __awaiter(this, void 0, void 0, function () {
            var paramsCommit_1, paramsDir_1, paramsTerminal_1, tasks_1, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log('Syncing cache with AWS - ' + hash);
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        paramsCommit_1 = {
                            s3Params: {
                                Bucket: options.bucket,
                                Key: hash + ".commit"
                            },
                            localFile: path_1.join(cacheDirectory, hash + ".commit")
                        };
                        paramsDir_1 = {
                            s3Params: {
                                Bucket: options.bucket,
                                Prefix: "" + hash
                            },
                            localDir: path_1.join(cacheDirectory, "" + hash)
                        };
                        paramsTerminal_1 = {
                            s3Params: {
                                Bucket: options.bucket,
                                Key: "terminalOutputs/" + hash
                            },
                            localFile: path_1.join(cacheDirectory, "terminalOutputs/" + hash)
                        };
                        tasks_1 = [];
                        tasks_1.push(new Promise(function (resolve) {
                            var uploderCommit = client.downloadFile(paramsCommit_1);
                            uploderCommit.on('end', function () {
                                console.log("Part1 - downloading...");
                                resolve(true);
                            });
                            uploderCommit.on('error', function (err) {
                                console.error(err);
                                resolve(true);
                            });
                        }));
                        tasks_1.push(new Promise(function (resolve) {
                            var uploderTerminal = client.downloadFile(paramsTerminal_1);
                            uploderTerminal.on('end', function () {
                                console.log("Part2 - downloading...");
                                resolve(true);
                            });
                            uploderTerminal.on('error', function (err) {
                                console.error(err);
                                resolve(true);
                            });
                        }));
                        tasks_1.push(new Promise(function (resolve) {
                            var uploderDir = client.downloadDir(paramsDir_1);
                            uploderDir.on('end', function () {
                                console.log("Part3 - downloading...");
                                resolve(true);
                            });
                            uploderDir.on('error', function (err) {
                                console.error(err);
                                resolve(true);
                            });
                        }));
                        return [4 /*yield*/, Promise.all(tasks_1)];
                    case 2:
                        _a.sent();
                        console.log('Cache finished downloading.');
                        return [2 /*return*/, true];
                    case 3:
                        e_1 = _a.sent();
                        console.log(e_1);
                        console.log("WARNING: failed to download cache from " + options.bucket + ": " + e_1.message);
                        return [2 /*return*/, false];
                    case 4: return [2 /*return*/];
                }
            });
        });
    }
    function store(hash, cacheDirectory) {
        return __awaiter(this, void 0, void 0, function () {
            var paramsDir_2, paramsCommit_2, paramsTerminal_2, tasks_2, e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        console.log('Syncing cache with AWS - ' + hash);
                        paramsDir_2 = {
                            deleteRemoved: false,
                            s3Params: {
                                Bucket: options.bucket,
                                Prefix: "" + hash
                            },
                            localDir: path_1.join(cacheDirectory, "" + hash)
                        };
                        paramsCommit_2 = {
                            deleteRemoved: false,
                            s3Params: {
                                Bucket: options.bucket,
                                Key: hash + ".commit"
                            },
                            localFile: path_1.join(cacheDirectory, hash + ".commit")
                        };
                        paramsTerminal_2 = {
                            deleteRemoved: false,
                            s3Params: {
                                Bucket: options.bucket,
                                Key: "terminalOutputs/" + hash
                            },
                            localFile: path_1.join(cacheDirectory, "terminalOutputs/" + hash)
                        };
                        tasks_2 = [];
                        tasks_2.push(new Promise(function (resolve) {
                            var uploderCommit = client.uploadFile(paramsCommit_2);
                            uploderCommit.on('end', function () {
                                console.log("Part1 - uploading...");
                                resolve(true);
                            });
                            uploderCommit.on('error', function (err) {
                                console.error(err);
                                resolve(true);
                            });
                        }));
                        tasks_2.push(new Promise(function (resolve) {
                            var uploderTerminal = client.uploadFile(paramsTerminal_2);
                            uploderTerminal.on('end', function () {
                                console.log("Part2 - uploading...");
                                resolve(true);
                            });
                            uploderTerminal.on('error', function (err) {
                                console.error(err);
                                resolve(true);
                            });
                        }));
                        tasks_2.push(new Promise(function (resolve) {
                            var uploderDir = client.uploadDir(paramsDir_2);
                            uploderDir.on('end', function () {
                                console.log("Part3 - uploading...");
                                resolve(true);
                            });
                            uploderDir.on('error', function (err) {
                                console.error(err);
                                resolve(true);
                            });
                        }));
                        return [4 /*yield*/, Promise.all(tasks_2)];
                    case 1:
                        _a.sent();
                        console.log("Cache uploaded");
                        return [2 /*return*/, true];
                    case 2:
                        e_2 = _a.sent();
                        console.log("WARNING: failed to upload cache to " + options.bucket + ": " + e_2.message);
                        return [2 /*return*/, false];
                    case 3: return [2 /*return*/];
                }
            });
        });
    }
}
exports["default"] = runner;

# nx-remotecache-aws

A task runner for [@nrwl/nx](https://nx.dev/react) that uses AWS S3 as a remote cache, so all team members and CI servers can share a single cache. The concept and benefits of [computation caching](https://nx.dev/angular/guides/computation-caching) are explained in the NX documentation.

## setup

```
npm install --save-dev git+ssh://git@bitbucket.org:openvantage/nx-remotecache-aws.git
```

finally, add `tasksRunnerOptions` in your `nx.json` file

```json
{
    "projects": {
        ...
    },
    "tasksRunnerOptions": {
        "default": {
            "runner": "nx-remotecache-aws",
            "options": {
                "bucket": "nx-cache",
                "accessKeyId": "",
                "secretAccessKey": "",
                "region": "af-south-1",
              "cacheableOperations": [
                "build",
                "test",
                "lint",
                "e2e"
              ]
            }
        }
    }
}

```

run a build and see if files end up in your cache storage bucket:

```
nx run-many --target=build --all
```
